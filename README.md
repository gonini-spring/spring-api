# Transação API

API em Java com Framework Spring para gerenciamento de transações financeiras.

### Funcionamento da API
A API deve criar e deletar transações financeiras. Além disso, deve gerar cálculos estatísticos sobre as transações criadas nos últimos 60 segundos.

#### Endpoints
 `1. Post  /transacao`

Este é o endpoint que irá receber as Transações. Cada transação consiste de um *valor* e uma *dataHora* de quando ela aconteceu:

#### Body

~~~
  {
	 "valor": 10.9848,
	 "dataHora": "2020-09-10T21:07:40.603"
  }
~~~

**valor** - Valor em decimal com ponto flutuante da transação.<br />
**dataHora** - Data/Hora no padrão ISO em que a transação aconteceu.<br />

#### Retorno

- Transação aceita: *201 Created* sem corpo;<br />
- Transação recusada: *422 Unprocessable Entity* retornar JSON que informa quais erros de validação foram encontrados;<br />
- Transação com erro no JSON: *400 Bad Request*.<br />

#### Requisitos

- Toda transação DEVE ter valor e dataHora;
- Uma transação NÃO DEVE acontecer no futuro;
- Uma transação DEVE ter acontecido a qualquer momento no passado;
- Uma transação NÃO DEVE ter valor negativo;
- Uma transação DEVE ter valor igual ou maior que 0 (zero).

 `2. DELETE /transacao`
 
 Esta requisição simplesmente apaga todos os dados de transação que estejam armazenados.
 
 `3. GET /estatistica`
 
 Deve retornar estatísticas das transações recebidas nos últimos 60 segundos, com base no campo dataHoraEnvio(campo interno da API Rest para controle da gravação do JSON enviado) que recebe automaticamente o horário de gravação da Transação no servidor .

Veja a seguir um exemplo de resposta esperado desse endpoint:
 
~~~
 {
	"count": 10,
	"sum": 1234.56,
	"avg": 123.456,
	"min": 12.34,
	"max": 123.56
}
~~~

**count** - Quantidade de transações nos últimos 60 segundos. <br />
**sum** - Soma total do valor transactionado nos últimos 60 segundos. <br />
**avg** - Média do valor transacionado nos últimos 60 segundos. <br />
**min** - Menor valor transacionado nos últimos 60 segundos. <br />
**max** - Maior valor transacionado nos últimos 60 segundos. <br />

`4. GET /transacao`

Esta requisição retorna todos os dados de transação que estejam armazenados.

Exemplo do retorno esperado:

~~~
[
  {
    "valor": 50.56,
    "dataHora": "2020-09-17T22:07:40.603"
  },
  {
    "valor": 412.56,
    "dataHora": "2020-09-17T22:07:40.603"
  },
  {
    "valor": 370.86,
    "dataHora": "2020-09-17T22:07:40.603"
  },
  {
    "valor": 215.33,
    "dataHora": "2020-09-17T22:07:40.603"
  },
  {
    "valor": 199.66,
    "dataHora": "2020-09-17T22:07:40.603"
  }
]
~~~

`5. GET /actuator`

Exibe os endpoints do actuator que estão ativados.

~~~
{
  "_links": {
    "self": {
      "href": "http://localhost:8080/actuator",
      "templated": false
    },
    "health": {
      "href": "http://localhost:8080/actuator/health",
      "templated": false
    },
    "health-path": {
      "href": "http://localhost:8080/actuator/health/{*path}",
      "templated": true
    },
    "info": {
      "href": "http://localhost:8080/actuator/info",
      "templated": false
    }
  }
}
~~~


`5-a. /actuator/info`

 Retorna informações gerais.
 
~~~
{
  "app": {
    "name": "transacao",
    "description": "API para transacoes com Spring Boot",
    "version": "0.0.1-SNAPSHOT",
    "encoding": "UTF-8",
    "java": {
      "version": "1.8.0_261"
    }
  }
}
~~~


`5-b. /actuator/health`

Retorna informações de integridade do aplicativo.

~~~
{
  "status": "UP",
  "components": {
    "diskSpace": {
      "status": "UP",
      "details": {
        "total": 142506717184,
        "free": 10296442880,
        "threshold": 10485760
      }
    },
    "ping": {
      "status": "UP"
    }
  }
}
~~~

`5-c. /actuator/prometheus`

Retorna métricas da API.

`5-d. /actuator/metrics`

Retorna lista com métricas mais detalhadas da API.

Abaixo exemplo de retorno com as métricas:

~~~
{
  "names": [
    "jvm.memory.max",
    "jvm.threads.states",
    "jvm.gc.memory.promoted",
    "jvm.memory.used",
    "jvm.gc.max.data.size",
    "jvm.memory.committed",
    "system.cpu.count",
    "logback.events",
    "http.server.requests",
    "jvm.buffer.memory.used",
    "tomcat.sessions.created",
    "jvm.threads.daemon",
    "system.cpu.usage",
    "jvm.gc.memory.allocated",
    "tomcat.sessions.expired",
    "jvm.threads.live",
    "jvm.threads.peak",
    "process.uptime",
    "tomcat.sessions.rejected",
    "process.cpu.usage",
    "jvm.classes.loaded",
    "jvm.gc.pause",
    "jvm.classes.unloaded",
    "tomcat.sessions.active.current",
    "tomcat.sessions.alive.max",
    "jvm.gc.live.data.size",
    "jvm.buffer.count",
    "jvm.buffer.total.capacity",
    "tomcat.sessions.active.max",
    "process.start.time"
  ]
}
~~~

Para acessar as métricas mais detalhadas: `/actuator/metrics/<nomeDaMetrica>`

#### Swagger
Para acessá-lo, abrir o link abaixo no navegador.

```
http://localhost:8080/swagger-ui.html
```


#### Testes

* Para executar o teste unitário:

```
mvn test
```

* Para executar todos os testes (incluindo o de integração):

```
mvn integration-test
```

#### Execução

Para rodar a API via terminal

```
mvn spring-boot:run
```

Por padrão a API está no endereço [http://localhost:8080/](http://localhost:8080/)